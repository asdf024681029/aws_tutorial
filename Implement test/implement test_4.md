# Training Exam-Architecting on AWS 

## 1. Build a two-tier web application 

### Scenario

<p align="center">
    <img src="IMAGES/implement_4-1.png" width="60%" height="60%">
</p>

- Region : in **Virginia**
- Allow engineers access to the instances via Session Manager 
(Please take a screenshot for each requirement)

### Requirement

- VPC requirement 
    - IPv4 CIDR : 10.0.0.0/16
    - Private Subnet 1 : us-east-1a
    - Public Subnet 1 : us-east-1a
    - Private Subnet 2 : us-east-1b
    - Public Subnet 2 : us-east-1b
    - VPC DNS hostnames : Enable
    - Public subnets auto-assign public IP : Enable
    - NAT Gateway : 1 in whole VPC
- ELB requirement 
    - Load Balancer : Application Load Balancer
    - Load Balancer Protocol : HTTP
    - Load Balancer Listener : 80/8000
    - VPC Availability Zones : us-east-1a & us-east-1b
- EC2 AutoScaling Group requirement
    - AMI : Amazon Linux AMI (HVM)
    - Instance Type : t2.micro
    - subnet : Private subnet 1&2
    - Port : 80/443/22 from ALB
    - Tag : **Name/Yourname**
    - Min : 2 instances
    - Max : 4 instances
    - Scaling Policy : Target set as 80%
    - Type : 8G SSD(gp2) 
    - Encrypt : KMS key
    - Purchase Option : Spot
    - Service Requirements :
        - Session Manager enabled
        - Apache with welcome page
        - Trigger SNS with E-mail when CloudWatch alert

## 2. Host Static website

### Scenario

Use S3, host a static website, allow users to access the website content via the internet 

<p align="center">
    <img src="IMAGES/implement_4-2.png" width="60%" height="60%">
</p>

Please host two static website separately to confirm that both website are displayed normally.

And to load the content from the second static website into the first static website, please remember to modify the content of the first static website, as shown below:

>   < html> <br>
>    ... <br>
>    ... <br>
>    ... <br>
>      < script type="text/javascript"> <br>
>                  $("#loadDiv").load( <br>
>                  "http://< Your-Second-website>" <br>
>             ); <br>
>     < /script> <br>
>    < /html> <br>

Please resolve CORS problem, allow the first static website to display the contents of the second static website 


<p align="center">
    <img src="IMAGES/implement_4-3.jpeg" width="60%" height="60%">
</p>

## 3. Route 53 Failover 

### Scenario

The customer requires the website to be highly available and to withstand failover while also being able to back up the data at the lowest cost.  

<p align="center">
    <img src="IMAGES/implement_4-4.png" width="60%" height="60%">
</p>

### Reuqirement 

- Use **Route 53** to complete **Health Check** (One Health Check/10s), use **DNS Failover** to redirect traffic to another region’s S3 


## 4. Upload the examination paper:

<p align="center">
    <img src="IMAGES/implement_4-5.png" width="60%" height="60%">
</p>

### Requirement 
- Upload the file name as : **implement-test-Jonny.docx**
- Please create your own bucket (Source side)
- Make sure the Lambda function follows this order 
    - SNS 
    - DynamoDB
    - Copy to S3 bucket 
- Using one of the created SNS Topic from **ect-implement-test**
- Under the Publish’s message, please write : **< Your-Name> has done the sns part**
- Using DynamoDB Table from **ect-implement-test-part3**
- The structure of the Table ：
```
{ 
    "Name": "Jonny", 
    "Message": "Jonny has done the dynamodb part",
    "Time": "2019-08-27 20:17:35" 
} 
```
- Make sure that Partition key is **Name**. And make sure that the data type  for the column **Name**, **Message** and **Time** is String 
- Make sure that the format of your message is the same as the one in the above example 
- Make sure that bucket triggered by Lambda function is the bucket with Name **ect-implement-test-upload** (Destination side)
- Lambda Code sample :
``` python 
import json 
import boto3 
import time 
def lambda_handler(event, context): 
    # TODO implement
    extract_first = event['Records'][0]['s3']['object']['key']
    extract_sec = extract_first.split('-', 2)[2] 
    examner = extract_sec.split('.')[0] 
    t = time.time() + 60*60*8 
    st = time.localtime(t) 
    uploadtime = time.strftime('%Y-%m-%d %H:%M:%S', st) 
    Message = examner + ' has done the sns part' 
    dest_bucket = 'ect-implement-test-upload' 
    dest_key = '20190829/' + event['Records'][0]['s3']['object']['key'] 
    ...  
    ... 
    ... 
    return { 
        'statusCode': 200, 
        'body': json.dumps('Hello from Lambda!')
    } 
```

## END
### **Please clean up all resources after answering all questions**