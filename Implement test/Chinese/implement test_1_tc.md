# Training Exam-Architecting on AWS 

## 1. Build a two-tier web application 

### Scenario

<p align="center">
    <img src="IMAGES/implement_1-1.png" width="60%" height="60%">
</p>

- Region: **Virginia** 建立了兩層式Web應用程式
- Engineer can access instances via Session Manager
（請針對各項需求進行截圖）

### 需求

- VPC 規格要求
    - IPv4 CIDR : 10.0.0.0/16
    - Private Subnet 1 : us-east-1a
    - Public Subnet 1 : us-east-1a
    - Private Subnet 2 : us-east-1b
    - Public Subnet 2 : us-east-1b
    - VPC DNS hostnames : Enable
    - Public subnets auto-assign public IP : Enable
    - NAT Gateway : 1 in whole VPC
- ELB 規格要求
    - Load Balancer : Application Load Balancer
    - Load Balancer Protocol : HTTP
    - Load Balancer Listener : 80/8000
    - VPC Availability Zones : us-east-1a & us-east-1b
- EC2 AutoScaling Group 規格要求
    - AMI : Amazon Linux AMI (HVM)
    - Instance Type : t2.micro
    - subnet : Private subnet 1&2
    - Port : 80/443/22 from ALB
    - Tag : **Name/Yourname**
    - Min : 2 instances
    - Max : 4 instances
    - Scaling Policy : Target set as 80%
    - Type : 8G SSD(gp2) 
    - Encrypt : KMS key
    - Purchase Option : Spot
    - Service Requirements :
        - Session Manager enabled
        - Apache with welcome page
        - Trigger SNS with E-mail when CloudWatch alert


## 2. Customers want to expand their business to Oregon

### Scenario 

客戶想迅速將業務橫跨到Region : **Oregon地區**
需求：只需要在Oregon 開一台EC2，不用ELB、Auto Scaling等。
（截圖Oregon 的 Instance 與網頁）

<p align="center">
    <img src="IMAGES/implement_1-2.png" width="60%" height="60%">
</p>

### 需求

- VPC 規格要求
    - IPv4 CIDR : 10.0.0.0/16
    - Private Subnet 1 : us-west-2a
    - Public Subnet 1 : us-west-2a
- EC2 規格要求
    - AMI : 上一題已有Web Server 之AMI
    - Instance Type : t2.micro
    - subnet : us-west-2a
    - Port : 80/443/22


## 3. EFS file sharing 

### Scenario

客戶想讓Virginia中的EC2透過**EFS共享資料**，接著從一台EC2的EFS 資料夾中創立一個**txt檔**後切換到第二台EC2，驗證EFS是否掛載成功

<p align="center">
    <img src="IMAGES/implement_1-3.png" width="60%" height="60%">
</p>

### 需求

- EFS 規格要求
    - Throughput mode : Bursting
    - Performance mode : General Purpose
    - Encryption : Enable



## 4. Route 53 Failover 

### Scenario 

客戶要求網站需要高可用性並能夠容錯移轉，同時做到備份資料的功能，要最低成本，想利用**Route 53** 完成。

<p align="center">
    <img src="IMAGES/implement_1-4.png" width="60%" height="60%">
</p>

### 需求

- 利用**Route 53** 做**Health Check** (10s一次)，進行 **Failover** 將流量導到其他EC2。

- 兩個VPC，VPC 1(primary)是第一題的環境；VPC 2(failover)是第二題的環境，public subnet放置web server，作為發生錯誤時，要redirect的機器。


## END
### **答題完畢後請清理資源**