# Training Exam-Architecting on AWS 

## 1. Build a two-tier web application 

### Scenario

<p align="center">
    <img src="IMAGES/implement_3-1.png" width="60%" height="60%">
</p>

- Region: **Virginia** 建立了兩層式Web應用程式
- Engineer can access instances via Session Manager
（請針對各項需求進行截圖）

### 需求

- VPC 規格要求
    - IPv4 CIDR : 10.0.0.0/16
    - Private Subnet 1 : us-east-1a
    - Public Subnet 1 : us-east-1a
    - Private Subnet 2 : us-east-1b
    - Public Subnet 2 : us-east-1b
    - VPC DNS hostnames : Enable
    - Public subnets auto-assign public IP : Enable
    - NAT Gateway : 1 in whole VPC
- ELB 規格要求
    - Load Balancer : Application Load Balancer
    - Load Balancer Protocol : HTTP
    - Load Balancer Listener : 80/8000
    - VPC Availability Zones : us-east-1a & us-east-1b
- EC2 AutoScaling Group 規格要求
    - AMI : Amazon Linux AMI (HVM)
    - Instance Type : t2.micro
    - subnet : Private subnet 1&2
    - Port : 80/443/22 from ALB
    - Tag : **Name/Yourname**
    - Min : 2 instances
    - Max : 4 instances
    - Scaling Policy : Target set as 80%
    - Type : 8G SSD(gp2) 
    - Encrypt : KMS key
    - Purchase Option : Spot
    - Service Requirements :
        - Session Manager enabled
        - Apache with welcome page
        - Trigger SNS with E-mail when CloudWatch alert


## 2. Using CloudFront to reuduce latency 

### Scenario
呈第一題，客戶希望降低讀取網頁的Latency，要求在web server前掛上**Cloudfront**。掛載完成之後，請驗證 Cloudfront 的 **response header** 有 **cache hit from cloudfront**。

<p align="center">
    <img src="IMAGES/implement_3-2.png" width="60%" height="60%">
</p>

### 需求
- Cloudfront 規格要求
    - Cloudfront log, header, cookie forward all : Enable
    - cache TTL
        - min : 0
        - max : one month
        - default : one day 

## 3. Check Failover 

### Scenario
呈第二題，當網頁fail，驗證錯誤時，CloudFront能導到第一題所設置的error page。


## 4. S3 Presigned URL

### Scenario
創立一個S3，當有新的圖片上傳，透過任意方式取得該物件的presigned URL。

## END
### **答題完畢後請清理資源**