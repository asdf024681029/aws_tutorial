# Training Exam-Architecting on AWS 

## 1. Build a two-tier web application 

### Scenario

<p align="center">
    <img src="IMAGES/implement_4-1.png" width="60%" height="60%">
</p>

- Region: **Virginia** 建立了兩層式Web應用程式
- Engineer can access instances via Session Manager
（請針對各項需求進行截圖）

### 需求

- VPC 規格要求
    - IPv4 CIDR : 10.0.0.0/16
    - Private Subnet 1 : us-east-1a
    - Public Subnet 1 : us-east-1a
    - Private Subnet 2 : us-east-1b
    - Public Subnet 2 : us-east-1b
    - VPC DNS hostnames : Enable
    - Public subnets auto-assign public IP : Enable
    - NAT Gateway : 1 in whole VPC
- ELB 規格要求
    - Load Balancer : Application Load Balancer
    - Load Balancer Protocol : HTTP
    - Load Balancer Listener : 80/8000
    - VPC Availability Zones : us-east-1a & us-east-1b
- EC2 AutoScaling Group 規格要求
    - AMI : Amazon Linux AMI (HVM)
    - Instance Type : t2.micro
    - subnet : Private subnet 1&2
    - Port : 80/443/22 from ALB
    - Tag : **Name/Yourname**
    - Min : 2 instances
    - Max : 4 instances
    - Scaling Policy : Target set as 80%
    - Type : 8G SSD(gp2) 
    - Encrypt : KMS key
    - Purchase Option : Spot
    - Service Requirements :
        - Session Manager enabled
        - Apache with welcome page
        - Trigger SNS with E-mail when CloudWatch alert


## 2. Using CloudFront to reuduce latency 

### Scenario

利用AWS S3，放置靜態網頁，使用者可由網路連進去看到網頁內容。

<p align="center">
    <img src="IMAGES/implement_4-2.png" width="60%" height="60%">
</p>

### 需求
- 請分別host兩個靜態網頁, 確認兩個網頁都顯示正常
- 並且在第一個靜態網頁當中載入第二個靜態網頁的內容, 請記得修改第一個靜態網頁的內容, 如下所示：
```
<html>
  ...
  ...
  ...
    <script type="text/javascript">
      $("#loadDiv").load(
        "http://<Your-Second-website>"
      );
    </script>
  </html>
</html>
```
- 請試著解決CORS的問題, 讓第一個靜態網頁能正常顯示載入第二個靜態網頁的內容

<p align="center">
    <img src="IMAGES/implement_4-3.jpeg" width="60%" height="60%">
</p>

## 3. Route 53 Failover 

### Scenario
客戶要求網站需要高可用性並能夠容錯移轉，同時做到備份資料的功能，要最低成本，想利用**Route 53** 完成。

<p align="center">
    <img src="IMAGES/implement_4-4.png" width="60%" height="60%">
</p>

### 需求

- 利用**Route 53**對第一題建置的環境（primary VPC）做**Health Check**（10s一次），進行 **Failover** 將流量導到位在另一個Region的S3。

## 4. 上傳考卷
<p align="center">
    <img src="IMAGES/implement_4-5.png" width="60%" height="60%">
</p>


- 上傳的檔名請如範例所示：**implement-test-Jonny.docx**
- Bucket請自己創建 (來源端)
- Lambda順序請依序完成 
    - SNS 
    - DynamoDB 
    - Copy to s3 bucket
- 其中SNS的Topic, 請使用已經創建好的 **ect-implement-test**
- Publish的訊息請寫：**< Your-Name> has done the sns part**
- DynamoDB Table 請使用 **ect-implement-test-part3**
- Table結構：
```
{
  "Name": "Jonny",
  "Message": "Jonny has done the dynamodb part",
  "Time": "2019-08-27 20:17:35"
}
```
- 其中Name為partition key, Message和 Time的資料型態為 String
- Message內容請如範例所示
- Copy用的Bucket請使用 **ect-implement-test-upload** (目的端)
- Lambda sample :
``` python 
import json 
import boto3 
import time 
def lambda_handler(event, context): 
    # TODO implement
    extract_first = event['Records'][0]['s3']['object']['key']
    extract_sec = extract_first.split('-', 2)[2] 
    examner = extract_sec.split('.')[0] 
    t = time.time() + 60*60*8 
    st = time.localtime(t) 
    uploadtime = time.strftime('%Y-%m-%d %H:%M:%S', st) 
    Message = examner + ' has done the sns part' 
    dest_bucket = 'ect-implement-test-upload' 
    dest_key = '20190829/' + event['Records'][0]['s3']['object']['key'] 
    ...  
    ... 
    ... 
    return { 
        'statusCode': 200, 
        'body': json.dumps('Hello from Lambda!')
    } 
```

## END
### **答題完畢後請清理資源**