# Training Exam-Architecting on AWS 

## 1. Build a two-tier web application 

### Scenario

<p align="center">
    <img src="IMAGES/implement_3-1.png" width="60%" height="60%">
</p>

- Region : in **Virginia**
- Allow engineers access to the instances via Session Manager 
(Please take a screenshot for each requirement)
### Requirement

- VPC requirement 
    - IPv4 CIDR : 10.0.0.0/16
    - Private Subnet 1 : us-east-1a
    - Public Subnet 1 : us-east-1a
    - Private Subnet 2 : us-east-1b
    - Public Subnet 2 : us-east-1b
    - VPC DNS hostnames : Enable
    - Public subnets auto-assign public IP : Enable
    - NAT Gateway : 1 in whole VPC
- ELB requirement 
    - Load Balancer : Application Load Balancer
    - Load Balancer Protocol : HTTP
    - Load Balancer Listener : 80/8000
    - VPC Availability Zones : us-east-1a & us-east-1b
- EC2 AutoScaling Group requirement
    - AMI : Amazon Linux AMI (HVM)
    - Instance Type : t2.micro
    - subnet : Private subnet 1&2
    - Port : 80/443/22 from ALB
    - Tag : **Name/Yourname**
    - Min : 2 instances
    - Max : 4 instances
    - Scaling Policy : Target set as 80%
    - Type : 8G SSD(gp2) 
    - Encrypt : KMS key
    - Purchase Option : Spot
    - Service Requirements :
        - Session Manager enabled
        - Apache with welcome page
        - Trigger SNS with E-mail when CloudWatch alert


## 2. Using CloudFront to reuduce latency 

### Scenario
From the first question, the customer hopes to reduce latency when a reading a website and requires the web server behind CloudFront.  After the mount is complete, verify that CloudFront’s **response header** has a **cache hit** from CloudFront.

<p align="center">
    <img src="IMAGES/implement_3-2.png" width="60%" height="60%">
</p>

### Requirement

- Cloudfront requirement
    - Cloudfront log, header, cookie forward all : Enable
    - cache TTL
        - min : 0
        - max : one month
        - default : one day 

## 3. Check Failover 

### Scenario
From the second question, when the website fails, you have to recognize the error, and make sure CloudFront is able to redirect to the error page in Questions 1.  

## 4. S3 Presigned URL

### Scenario
Create an S3.  When there are new pictures uploaded, get the presigned URL of the object (using any method).  

## END
### **Please clean up all resources after answering all questions**