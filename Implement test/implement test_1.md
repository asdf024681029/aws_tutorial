# Training Exam-Architecting on AWS 

## 1. Build a two-tier web application 

### Scenario

<p align="center">
    <img src="IMAGES/implement_1-1.png" width="60%" height="60%">
</p>

- Region : in **Virginia**
- Allow engineers access to the instances via Session Manager 
(Please take a screenshot for each requirement)

### Reuqirement 

- VPC requirement
    - IPv4 CIDR : 10.0.0.0/16
    - Private Subnet 1 : us-east-1a
    - Public Subnet 1 : us-east-1a
    - Private Subnet 2 : us-east-1b
    - Public Subnet 2 : us-east-1b
    - VPC DNS hostnames : Enable
    - Public subnets auto-assign public IP : Enable
    - NAT Gateway : 1 in whole VPC
- ELB requirement
    - Load Balancer : Application Load Balancer
    - Load Balancer Protocol : HTTP
    - Load Balancer Listener : 80/8000
    - VPC Availability Zones : us-east-1a & us-east-1b
- EC2 AutoScaling Group requirement
    - AMI : Amazon Linux AMI (HVM)
    - Instance Type : t2.micro
    - subnet : Private subnet 1&2
    - Port : 80/443/22 from ALB
    - Tag : **Name/Yourname**
    - Min : 2 instances
    - Max : 4 instances
    - Scaling Policy : Target set as 80%
    - Type : 8G SSD(gp2) 
    - Encrypt : KMS key
    - Purchase Option : Spot
    - Service Requirements :
        - Session Manager enabled
        - Apache with welcome page
        - Trigger SNS with E-mail when CloudWatch alert


## 2. Customers want to expand their business to Oregon

### Scenario 

They want to launch an EC2 instance, but do not want to use ELB and Auto Scaling.
(please snapshot the Instance in Oregon and the website )

<p align="center">
    <img src="IMAGES/implement_1-2.png" width="60%" height="60%">
</p>

### Reuqirement 

- VPC requirement
    - IPv4 CIDR : 10.0.0.0/16
    - Private Subnet 1 : us-west-2a
    - Public Subnet 1 : us-west-2a
- EC2 requirement
    - AMI : Use Question 1’s web Server’s AMI 
    - Instance Type : t2.micro
    - subnet : us-west-2a
    - Port : 80/443/22


## 3. EFS file sharing 

### Scenario

The customer wants the EC2 instance in Virginia to go through **EFS for file sharing**. From the first EC2’s EFS, create a **txt file** and access the second EC2 to verify that the EFS has successfully mounted 

<p align="center">
    <img src="IMAGES/implement_1-3.png" width="60%" height="60%">
</p>

### Reuqirement 

- EFS requirement 
    - Throughput mode : Bursting
    - Performance mode : General Purpose
    - Encryption : Enable



## 4. Route 53 Failover 

### Scenario 

The customer requires the website to be highly available and to **withstand failover** while also being able to back up the data at the lowest cost.  

<p align="center">
    <img src="IMAGES/implement_1-4.png" width="60%" height="60%">
</p>

### Reuqirement 

- Use **Route 53** to complete **Health Check** (One Health Check/10s), use **DNS Failover** to redirect traffic to another EC2 

- 2 VPCs, VPC 1 environment is the same as the one in the first question ; VPC 2 (Failover) environment is the same as the one in the second question. If the web server in the public subnet fails, redirect the web server 


## END
### **Please clean up all resources after answering all questions**